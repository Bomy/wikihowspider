# -*- coding: utf-8 -*-

import scrapy
from scrapy.http import FormRequest,Request,Response
import re
import os
import urllib
import urllib.parse
from wikihowspider.items import WikihowspiderItem
from wikihowspider import settings
import time
class WkhspiderSpider(scrapy.Spider):
    name = 'wkhspider'
    allowed_domains = ['zh.wikihow.com']
    start_urls = ['https://zh.wikihow.com/%E9%A6%96%E9%A1%B5/']
    BASE_URL = "https://zh.wikihow.com"
    articleindex = {}
    RST_DIR = "./rst"
    clsdic = {"next":1}
    clsfile = RST_DIR+"/cls.txt"
    def parse(self, response):
        links = response.xpath('//ul[@id="hp_categories"]/li/a/@href').extract()

        category = response.xpath('//ul[@id="hp_categories"]/li/a/text()').extract()
        print(category)
        for ct in links:
            url = self.BASE_URL + ct
            yield Request(url=url,callback=self.get_page_links,meta={"cls":ct,})
    #获取文章的链接
    def get_page_links(self,response):
        if response.url not in self.articleindex:
            self.articleindex[response.url] = 0
        artidx = self.articleindex[response.url]
        articlelinks = response.xpath('//div[@id="bodycontents"]/table/tr/td/div/a/@href').extract()
        nextindex = len(articlelinks)
        if nextindex > 0:
            artidx += len(articlelinks)
            self.articleindex[response.url] = artidx
            #print(response.url,str(artidx))
            for link in articlelinks:
                url = "https:"+link
                yield Request(url=url,callback=self.get_article,meta={"cls":response.meta["cls"],})
            data = {
                "restaction": "pull - chunk",
                "start": str(artidx)
            }
            #print("response 网址：",response.url)
            yield FormRequest(url=response.url,formdata=data,callback=self.get_page_links,meta={"cls":response.meta["cls"],})
    # 获取文章内容和图片，并存入数据库
    def get_article(self,response):
        i = 0;

        clas = urllib.parse.unquote(response.meta["cls"].split(":")[1])
        # 获取时间，因时间没有html标签，所以用正则表达式
        datereg = r"and timestamp (\d+)"
        dt = re.compile(datereg,re.S).findall(response.body.decode("utf-8","ignore"))[0][0:8]
        #获取标题
        title = response.xpath('//title/text()').extract()[0]
        #获取关键词
        keywords = response.xpath('//meta[@name="keywords"]/@content').extract()[0].replace(",","|").replace("WikiHow|","").replace("如何|","")
        #获取html文本中保存的内容
        bodycontents = response.xpath('//div[@id="bodycontents"]').extract()[0]
        replacestr = response.xpath('//div[@id="bodycontents"]/div').extract()
        #获取所有图片链接
        imgs = response.xpath('//div[@id="bodycontents"]/div/div/ol/li[contains(@class,"hasimage")]/div/a/div/img/@data-src').extract()
        #print("图片：",imgs)
        #判断是否创建根目录
        if not os.path.exists(self.RST_DIR):
            os.mkdir(self.RST_DIR)
        #判断是否创建一级目录（日期）
        dateDir = self.RST_DIR+"/"+dt
        if not os.path.exists(dateDir):
            os.mkdir(dateDir)
        #圈子id
        thisclasid = 0

        thisid = 0
        #二级目录（圈子id+yymmdd）
        clasname = None
        #查看该圈子是否已经爬取过了，如果没有爬去，则为他创建圈子id，并在根目录的cls.txt中登记
        if clas not in self.clsdic:
            #clsdic记录了圈子和圈子对应的id，next代表下一个圈子的id
            thisclasid = self.clsdic["next"]
            self.clsdic["next"] = self.clsdic["next"]+1
            clasname = "A" + ("000" + str(thisclasid))[-3:]
            with open(self.clsfile,"a") as clsf:
                clsf.write(clas+"      "+clasname)
                clsf.write("\n")
            self.clsdic[clas] = clasname
        else:
            clasname = self.clsdic[clas]
        #在每个圈子下记录每个文章的1清单文件
        with open(dateDir+"/list.csv","a+") as alfile:
            thisid = int(time.time()*1000)
            print("thisid",thisid)
            alfile.write(str(thisid)+","+str(len(imgs))+","+title+","+clasname+","+keywords)
            alfile.write("\n")

        CLSDIR = dateDir + "/" + clasname +"_"+dt
        if not os.path.exists(CLSDIR):
            os.mkdir(CLSDIR)
        MDIR = CLSDIR+"/"+title
        # 如果已经爬取过这个文章，则直接跳过
        if os.path.exists(MDIR):
            print(MDIR,"   is exsist,pass")
            return None
        os.mkdir(MDIR)
        IMG_DIR = MDIR+"/img"
        os.mkdir(IMG_DIR)
        for img in imgs:
            imgdata = None
            #计算图片id
            imageid = str(thisid)+"_img"+str(i)
            with open(IMG_DIR+"/"+imageid+".jpg","wb") as rstimg:
                print(img)
                try:
                    #爬取图片
                    imgdata = urllib.request.urlopen(img).read()
                except Exception as e:
                    with open(self.RST_DIR+"/error.txt","a") as errf:
                        errf.write(img + " " + IMG_DIR + "/" + str(i) + ".jpg")
                if imgdata:
                    #保存图片
                    rstimg.write(imgdata)
                    #下面两句是替换html中所有的img标签
                    scriptmgreg = r'<img.*?'+img+'.*?>'
                    bodycontents = re.sub(scriptmgreg,'<image id="'+imageid+'" src="'+img+'"></image>',bodycontents)

            with open(self.RST_DIR + "/path.txt", "a") as ptxt:
                ptxt.write(img + "      " + IMG_DIR + "/" + str(i) + ".jpg")
                ptxt.write("\n")
            i = i + 1
        content = str(bodycontents).replace(replacestr[-1], "").replace(replacestr[-2], "")
        if settings.USE_DB:
            contentitem = WikihowspiderItem()
            contentitem["title"] = urllib.parse.unquote(title)
            contentitem["content"] = content
            contentitem["cls"] = clas
            print(str(contentitem))
            yield contentitem
        else:
            try:
                with open(MDIR+"/"+str(thisid)+".html","w") as rstf:
                    rstf.write(content)
                with open(MDIR+"/"+str(thisid)+".txt","w") as txtf:
                    # 过滤掉所有的html标签和多余的换行标签
                    partten = "<script>.*?</script>"
                    onlytext = re.sub(partten,"",content,flags=re.S)
                    partten = "</?.*?>"
                    onlytext = re.sub(partten, "", onlytext, flags=re.S)
                    partten = "\n+\n"
                    onlytext = re.sub(partten, "\n", onlytext, flags=re.S)
                    txtf.write(onlytext)
            except Exception as e:
                    print(e.__str__())
