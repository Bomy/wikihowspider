# wikihowspider

该项目需要依赖一下第三方库：
pymysql
scrapy

项目配置：
在settings.py中配置数据库相关信息(DB开头)

项目启动方式：
进入项目根目录，然后scrapy crawl wkhspider

使用数据库存储文章，请修改settings.py中USE_DB为True
默认使用文件存储


数据库表：
| wkh   | CREATE TABLE `wkh` (
  `title` char(100) DEFAULT NULL,
  `content` text,
  `cls` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 
